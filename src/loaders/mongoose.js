'use strict';

const mongoose = require('mongoose');

const config = require('../config'),
  log = require('../helpers/log');

let connection = null;

const defaultOptions = {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true,
  promiseLibrary: global.Promise,
  connectTimeoutMS: 5000,
  bufferMaxEntries: 0
};

function init() {
  log.debug('mongodb initialize');
  return mongoose.connect(config.DATABASE.CONNECTION_STRING, defaultOptions)
    .then((result) => {
      log.debug('mongodb initialize success');
      connection = result.connection;
      return result.connection;
    });
}

function disconnect() {
  log.debug('mongodb disconnecting');
  return connection.close()
    .then(() => {
      connection = null;
      log.debug('mongodb disconnected');
    });
}

function getConnection() {
  if (connection) {
    return connection;
  }
  throw new Error('getConnection error: mongoose doesn\'t connect yet.');
}

module.exports = { init, disconnect, getConnection };
