'use strict';

const mongooseLoader = require('./mongoose');

module.exports = mongooseLoader.init()
  .then(() => require('./express'));
