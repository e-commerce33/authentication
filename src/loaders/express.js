'use strict';

const bodyParser = require('body-parser'),
  express = require('express'),
  morgan = require('morgan'),
  path = require('path'),
  swaggerParser = require('@apidevtools/swagger-parser'),
  swaggerRoutes = require('swagger-routes-express').connector,
  swaggerUi = require('swagger-ui-express');

const errorHandler = require('../services/errorHandler'),
  { errorObjects } = require('../constants/error'),
  log = require('../helpers/log');

function connectSwagger(app) {
  log.debug('connect swagger path.');

  const swaggerPath = path.resolve(__dirname, '..', 'routes', 'openapi.yaml');

  return swaggerParser.validate(swaggerPath)
    .then((schema) => {
      const connector = swaggerRoutes(require('../controllers'), schema);

      connector(app);
      app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(schema));
      app.use('/api-docs-json', (req, res) => res.json(schema));
    });
}

function pathNotFound(req, res, next) {
  const message = 'path not found';
  return next({ ...errorObjects.NOT_FOUND, message });
}

const app = express();

app.use(morgan('common'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

module.exports = connectSwagger(app)
  .then(() => {
    log.debug('add error handler');
    app.use(errorHandler);

    log.debug('add path not found handler');
    app.use(pathNotFound);
    return app;
  });

