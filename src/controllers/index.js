'use strict';

const { combine } = require('../helpers/api'),
  forms = require('./forms');

const combined = combine({
  forms
});

module.exports = combined;
