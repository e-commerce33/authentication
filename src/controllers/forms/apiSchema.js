'use strict';

const joi = require('joi');

const schema = {
  id: joi.string().hex().length(24),
  email: joi.string().email().trim(),
  password: joi.string().min(3).trim(),
  firstname: joi.string().min(1).max(50).trim(),
  lastname: joi.string().min(1).max(50).trim(),
  gender: joi.string().valid('male', 'famale')
};

module.exports = {
  register: {
    body: joi.object().keys({
      email: schema.email.required(),
      password: schema.password.required(),
      firstname: schema.firstname.required(),
      lastname: schema.lastname.required(),
      gender: schema.gender.required()
    })
  },
  login: {
    body: joi.object().keys({
      email: schema.email.required(),
      password: schema.password.required()
    })
  }
};
