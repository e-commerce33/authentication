/* eslint-disable func-names */
'use strict';

const mongoose = require('mongoose');

const { errorObjects } = require('../../constants/error'),
  mainDb = require('../../loaders/mongoose').getConnection(),
  passwordService = require('../../services/passwordService');

function errorDuplicateHandle(err, res, next) {
  if (err.name === 'MongoError' && err.code === 11000) {
    return next(errorObjects.CONFLICT);
  }

  return next(err);
}

const Schema = mongoose.Schema;

const authenticationSchema = new Schema({
  email: {
    type: String,
    required: true,
    index: { unique: true }
  },
  password: {
    type: String,
    minlength: 3,
    required: true
  }
}, { timestamps: true });

authenticationSchema.pre('save', function(next) {
  // eslint-disable-next-line no-invalid-this
  const that = this;

  if (!that.isModified('password')) {
    return next();
  }

  return passwordService.hashPassword(that.password)
    .then((hash) => {
      that.password = hash;
      return next();
    })
    .catch(next);
});

authenticationSchema.post('save', errorDuplicateHandle);

authenticationSchema.methods.comparePassword = function(candidatePassword) {
  // eslint-disable-next-line no-invalid-this
  return passwordService.comparePassword(candidatePassword, this.password);
};

module.exports = mainDb.model('Authentication', authenticationSchema, 'authentications');
