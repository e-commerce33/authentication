'use strict';

const _ = require('lodash');

const Model = require('./model'),
  apiSchema = require('./apiSchema'),
  config = require('../../config'),
  { errorObjects } = require('../../constants/error'),
  httpStatus = require('../../constants/httpStatus'),
  jwtService = require('../../services/jwtService'),
  log = require('../../helpers/log'),
  userService = require('../../services/userService');

function register(req, res, next) {
  log.debug('forms.register');
  let userData = null;

  return apiSchema.register.body.validateAsync(req.body)
    .then((value) => {
      userData = _.pick(value, ['email', 'firstname', 'lastname', 'gender']);
      const authData = _.pick(value, ['email', 'password']);
      return Model.create(authData);
    })
    .then(() => userService.create(userData))
    .then(() => jwtService.sign({ email: userData.email, type: 'access' }, config.ACCESS_SECRET))
    .then((accessToken) => res.status(httpStatus.CREATED).json({ accessToken }))
    .catch(next);
}

function login(req, res, next) {
  log.debug('forms.login');

  let reqBody = {};

  return apiSchema.login.body.validateAsync(req.body)
    .then((value) => {
      reqBody = value;
      return Model.findOne({ email: reqBody.email });
    })
    .then((user) => {
      if (user) {
        return user.comparePassword(reqBody.password);
      }

      const errObj = { ...errorObjects.NOT_FOUND, message: 'User not found' };
      throw errObj;
    })
    .then((passwordMatch) => {
      if (passwordMatch) {
        return jwtService.sign({ email: reqBody.email, type: 'access' }, config.ACCESS_SECRET);
      }
      const errObj = { ...errorObjects.UNAUTHORIZED,
        message: 'Please check your username and password.' };

      throw errObj;
    })
    .then((accessToken) => res.status(httpStatus.CREATED).json({ accessToken }))
    .catch(next);
}

module.exports = {
  register,
  login
};
