'use strict';

const apiCallerService = require('./apiCallerService'),
  config = require('../config'),
  jwtService = require('../services/jwtService'),
  log = require('../helpers/log'),
  { urlPathConcat } = require('../helpers/url');

function create(data) {
  log.debug('userService.create');
  const payload = { type: 'service', serviceName: config.serviceName };
  return jwtService.sign(payload, config.SERVICE_SECRET, { expiresIn: '5m' })
    .then((token) => {
      return apiCallerService.call(
        'POST',
        urlPathConcat(config.ENDPOINTS.USER, 'user'),
        { data },
        token
      );
    });
}

module.exports = { create };
