'use strict';

const bcrypt = require('bcryptjs');

const defaultRounds = 10;

function comparePassword(password, hashedPassword) {
  if (typeof password !== 'string' || typeof hashedPassword !== 'string') {
    throw new TypeError('Password must be string.');
  }

  return bcrypt.compare(password, hashedPassword);
}

function hashPassword(password, saltRounds = defaultRounds) {
  if (typeof password !== 'string') {
    throw new TypeError('Password must be string.');
  }
  if (!Number.isInteger(saltRounds)) {
    throw new TypeError('SaltRounds must be integer.');
  }
  if (saltRounds < 1) {
    throw new TypeError('SaltRounds must greater than zero.');
  }

  return bcrypt.hash(password, saltRounds);
}

module.exports = {
  comparePassword,
  hashPassword
};
