'use strict';

const axios = require('axios');
const config = require('../config');
const log = require('../helpers/log');

function call(method, url, { params = {}, data = {} } = {}, token = null) {
  const options = {
    method,
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'User-Agent': config.SERVICE_NAME
    },
    url,
    params,
    data
  };

  if (token) {
    options.headers.Authorization = `Bearer ${token}`;
  }

  log.debug('CALL:', options);
  return axios.request(options)
    .then((response) => {
      log.debug('REPLY:', response.data);
      return response.data;
    })
    .catch((err) => {
      log.error(`ERR RESPONSE: ${err}`);
      throw err;
    });
}

module.exports = {
  call
};
