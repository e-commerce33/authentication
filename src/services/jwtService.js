'use strict';

const jsonwebtoken = require('jsonwebtoken');

function sign(payload, secret, option = {}) {
  return new Promise((resolve, reject) => {
    jsonwebtoken.sign(payload, secret, option, (err, token) => {
      if (err) {
        return reject(err);
      }

      return resolve(token);
    });
  });
}

function verify(token, secret, option = {}) {
  return new Promise((resolve, reject) => {
    jsonwebtoken.verify(token, secret, option, (err, payload) => {
      if (err) {
        err.jwt = true;
        return reject(err);
      }

      return resolve(payload);
    });
  });
}

module.exports = {
  sign, verify
};
