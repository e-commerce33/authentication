'use strict';

const http = require('http');

const config = require('./config'),
  createServer = require('./loaders'),
  log = require('./helpers/log'),
  mongooseHandler = require('./loaders/mongoose');

const port = config.PORT;
let server = null;

// Event listener for HTTP server "error" event.
function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string' ? `Pipe ${port}` : `Port ${port}`;

  // handle specific listen errors with friendly messages
  switch (error.code) {
  case 'EACCES':
    log.error(`${bind} requires elevated privileges`);
    break;
  case 'EADDRINUSE':
    log.error(`${bind} is already in use`);
    break;
  default:
    log.error(error);
  }

  throw error;
}

// Event listener for HTTP server "listening" event.
function onListening() {
  const addr = server.address();
  const bind = typeof addr === 'string' ? `pipe ${addr}` : `port ${addr.port}`;

  log.info(`Listening on ${bind}`);
}

function onTerminating(signal) {
  log.info(`'${signal}' signal received.`);
  log.info('Draining request(s) and closing http server');

  return server.close(() => {
    log.info('Http server closed.');
    return mongooseHandler.disconnect()
      .then(() => {
        log.info('gracefully shutdown completed');
        process.exit(0);
      });
  });
}

createServer
  .then((app) => {
    app.set('port', port);

    server = http.createServer(app);
    // Listen on provided port, on all network interfaces.
    server.listen(port);

    server.on('listening', onListening);
    server.on('error', onError);

    // handle terminating signal from outside
    process.on('SIGTERM', onTerminating);
    process.on('SIGINT', onTerminating);
    process.on('SIGQUIT', onTerminating);
    process.on('SIGTSTP', onTerminating);
  })
  .catch((error) => {
    log.error(error);
    process.exit(1);
  });

