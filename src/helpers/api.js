'use strict';

const _ = require('lodash');

function combine(apis) {
  return _.reduce(apis, (out, ctrl, ctrlName) => {
    _.forEach(ctrl, (func, method) => {
      out[[ctrlName, method].join('.')] = func;
    });

    return out;
  }, {});
}

module.exports = { combine };
