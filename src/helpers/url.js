'use strict';

function urlPathConcat(first, ...rest) {
  return rest.reduce((output, value) => {
    return [
      output.replace(/\/$/u, ''),
      String(value).valueOf().replace(/^\//u, '')
    ].join('/');
  }, String(first).valueOf());
}

module.exports = { urlPathConcat };
