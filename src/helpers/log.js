'use strict';

const { createLogger, format, transports } = require('winston');

const config = require('../config');

const logger = createLogger({
  level: config.LOG.LEVEL,
  format: format.combine(
    format.colorize(),
    format.timestamp(),
    format.ms(),
    format.printf((info) => `${info.timestamp} ${info.level}: ${info.message} ${info.ms}`)
  ),
  transports: [new transports.Console()]
});

module.exports = logger;
